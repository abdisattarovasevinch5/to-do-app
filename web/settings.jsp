<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 02/08/2022
  Time: 10:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Setting</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>

<div class="container mt-4 mb-4 p-3 d-flex justify-content-center">
    <div class="card p-4">
        <div class=" image d-flex flex-column justify-content-center align-items-center">
            <button class="btn btn-secondary">
                <img src="https://i.imgur.com/wvxPV9S.png" height="100" width="100"/></button>
            <span class="name mt-3">
                ${user.getFullName()}</span> <span class="idd">@${user.getUsername()}</span>
            <div>
                <a href=/editUser?id=${user.getId()} class="btn btn-primary my-2" style=
                        "margin-left: 16px; float: left; margin-bottom: 2rem;"
                   onMouseOver="this.style.color='#0F0'"
                   onMouseOut="this.style.color='#00F'">
                    <i class="fas fa-edit"></i> Edit profile  </a>
            </div>
            <div>
                  <a href=/deleteUser?id=${user.getId()} class="btn btn-danger my-2" style=
                        "margin-left: 16px; float: left; margin-bottom: 2rem"
                   onMouseOver="this.style.color='#0F0'"
                   onMouseOut="this.style.color='#00F'">
                    <i class="fas fa-trash-alt"></i> Delete profile</a>
            </div>
        </div>
    </div>
</div>
</body>
</html>
