<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 02/08/2022
  Time: 9:20 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

</head>
<body>
<div class="row">
    <div class="col-md-6 offset-3">
        <form action="/users" method="post">

            <div class="form-group">
                <label for="full_name">Full name: </label>
                <input name="full_name" type="text" class="form-control" id="full_name"
                       placeholder="Input your full name here">
            </div>
            <div class="form-group">
                <label for="username">Username: </label>
                <input name="username" type="text" class="form-control" id="username"
                       placeholder="Input your username here">
            </div>
            <div class="form-group">
                <label for="password">Password: </label>
                <input name="password" type="password" class="form-control" id="password"
                       placeholder="Input your password here">
            </div>
            <button type="submit" class="btn btn-success my-3">Save</button>
        </form>
    </div>
</div>
</body>
</html>
