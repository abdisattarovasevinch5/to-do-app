<%@ taglib prefix="c-rt" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 02/07/2022
  Time: 11:56 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Main page</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
<div>
    <c-rt:choose>
        <c-rt:when test="${empty param.message}">
        </c-rt:when>
        <c-rt:otherwise>
            <c-rt:choose>
                <c-rt:when test="${param.success==true}">
                    <h1 style="color:darkgreen;">${param.message}</h1>
                </c-rt:when>
                <c-rt:when test="${param.success==false}">
                    <h1 style="color:red;">${param.message}</h1>
                </c-rt:when>
            </c-rt:choose>
        </c-rt:otherwise>

    </c-rt:choose>
</div>

<c-rt:choose>
    <c-rt:when test="${empty param.login}">
        <div style="justify-content: space-between">
            <div>
                <a href="/login.html" class="btn btn-primary" style=
                        "margin-left: 16px; float: left; margin-bottom: 2rem;justify-content: right"
                   onMouseOver="this.style.color='#0F0'"
                   onMouseOut="this.style.color='#00F'">
                    <i class="fas fa-sign-in-alt"></i>Login</a>
            </div>
            <div style="align-content: end">
                <a href="/user-form.jsp" class="btn btn-primary" style=
                        "margin-left: 16px; float: left; margin-bottom: 2rem"
                   onMouseOver="this.style.color='#0F0'"
                   onMouseOut="this.style.color='#00F'">
                    <i class="fas fa-user-plus"></i>Sign up</a>
            </div>
        </div>
    </c-rt:when>
    <c-rt:otherwise>
        <c-rt:choose>
            <c-rt:when test="${param.login==true}">
                <div style="justify-content: space-between">
                    <div>
                        <a href="/tasks" class="btn btn-primary" style=
                                "margin-left: 16px; float: left; margin-bottom: 2rem"
                           onMouseOver="this.style.color='#0F0'"
                           onMouseOut="this.style.color='#00F'">
                            <i class="fas fa-user"></i>Profile</a>
                    </div>
                    <div>
                        <a href="/logout" class="btn btn-primary" style=
                                "margin-left: 16px; float: left; margin-bottom: 2rem"
                           onMouseOver="this.style.color='red'"
                           onMouseOut="this.style.color='#00F'">
                            <i class="fas fa-sign-out-alt"></i>Logout</a>
                    </div>
                </div>
            </c-rt:when>
            <c-rt:when test="${param.login==false}">
                <div style="justify-content: space-between">
                    <a href="/login.html" class="btn btn-primary" style=
                            "margin-left: 16px; float: left; margin-bottom: 2rem"
                       onMouseOver="this.style.color='#0F0'"
                       onMouseOut="this.style.color='#00F'">
                        <i class="fas fa-sign-in-alt"></i>Login</a>
                </div>
            </c-rt:when>
        </c-rt:choose>
    </c-rt:otherwise>

</c-rt:choose>
</body>
</html>


