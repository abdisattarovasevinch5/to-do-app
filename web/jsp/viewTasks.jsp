<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 02/07/2022
  Time: 9:22 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c-rt" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${title}</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>

<div>
    <c-rt:choose>
        <c-rt:when test="${empty param.message}">
        </c-rt:when>
        <c-rt:otherwise>
            <c-rt:choose>
                <c-rt:when test="${param.success==true}">
                    <h1 style="color:darkgreen;">${param.message}</h1>
                </c-rt:when>
                <c-rt:when test="${param.success==false}">
                    <h1 style="color:red;">${param.message}</h1>
                </c-rt:when>
            </c-rt:choose>
        </c-rt:otherwise>

    </c-rt:choose>
</div>
<div class="container" style="padding-top: 2rem">
</div>
<div class="container d-flex justify-content-around" style="padding-top: 1%;">
    <div class="row">
        <a href='/jsp/task-form.jsp' class="btn btn-primary" style=
                "margin-left: 16px; float: left; margin-bottom: 2rem"
           onMouseOver="this.style.color='#0F0'"
           onMouseOut="this.style.color='#00F'">
            <i class="fas fa-plus"></i> Add Task</a>
        <a href="/logout" class="btn btn-danger" style=
                "margin-left: 16px; float: right; margin-bottom: 2rem"
           onMouseOver="this.style.color='#0F0'"
           onMouseOut="this.style.color='#00F'">
            <i class="fas fa-sign-out-alt"></i>Logout
        </a>
        <a href='/users'  class="btn btn-primary" style=
                "margin-left: 800px; float: left; margin-bottom: 2rem"
           onMouseOver="this.style.color='#0F0'"
           onMouseOut="this.style.color='#00F'">
            <i class="fas fa-user-cog"></i> Settings</a>
        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                <tr style="text-align: center">
                    <th scope="col">#</th>
                    <th scope="col">Task</th>
                  <%--  <th scope="col">Description</th>--%>
                    <th scope="col">Deadline</th>
                    <th scope="col" colspan="2">Settings</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${taskList}" var="task" varStatus="loop">
                    <tr scope="row" style="text-align: center">
                        <td>${loop.count}</td>
                        <td>${task.title}</td>
                            <%--  <td><a href=showDescription?id=${task.id} class="btn btn-success"><i class="fas fa-eye"></i>
                              </a></td>--%>
                        <td>${task.deadline}</td>
                        <td><a class="btn btn-info" href=editTask?id=${task.id}><i class="fas fa-edit"></i>
                        </a></td>
                        <td><a class="btn btn-danger" href=deleteTask?id=${task.id}><i
                                class="fas fa-trash"></i> </a></td>
                    </tr>
                </c:forEach>
                </tbody>
                <tbody></tbody>
                <tbody></tbody>
            </table>
            <div style="padding-left:40%;padding-right:40%;padding-top:1rem">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <c:forEach var="page" begin="1" end="${pages}">
                            <a href='tasks?page=${page-1}'>
                                <button>${page}</button>
                            </a>
                        </c:forEach>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>
</html>