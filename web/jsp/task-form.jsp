<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 02/07/2022
  Time: 9:41 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>TASK LIST</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>


<c:set var="headline" value="Add new task"/>

<c:if test="${task.id !=null}">
    <c:set var="headline" value="Edit task"/>
</c:if>
<div>
    <h1>${headline}</h1>
</div>
<div class="row">
    <div class="col-md-6 offset-3">
        <form action="/tasks" method="post">

            <c:if test="${task.id !=null}">
                <div class="form-group">
                    <input hidden name="id" type="text" class="form-control" id="taskId"
                           value="${task.id}">
                </div>
            </c:if>

            <div class="form-group">
                <label for="taskTitle">Title: </label>
                <input
                <c:if test="${task.id !=null}">
                        value="${task.title}"
                </c:if>

                        name="title" type="text" class="form-control" id="taskTitle"
                        placeholder="Input task title here">
            </div>


            <div class="form-group">
                <label for="taskDescription">Description: </label>
                <textarea
                        name="description" class="form-control" id="taskDescription"
                        placeholder="Input task description here"><c:if
                        test="${task.description !=null}">${task.description}</c:if></textarea>
            </div>

            <div class="form-group">
                <label for="taskDeadline">Deadline: </label>
                <input
                <c:if test="${task.deadline !=null}">
                        value="${task.deadline}"
                </c:if>
                        name="deadline" type="datetime-local" class="form-control" id="taskDeadline">
            </div>
            <div class="form-check my-4">
                <label class="form-check-label" for="exampleCheck1">Is Done?:</label>
                <input
                <c:if test="${task.status}">
                        checked
                </c:if>
                        name="status" type="checkbox" class="form-check-input" id="exampleCheck1">
            </div>
            <button type="submit" class="btn btn-success">Save</button>
        </form>
    </div>
</div>


</body>
</html>

