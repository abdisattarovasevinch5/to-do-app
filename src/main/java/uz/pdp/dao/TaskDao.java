package uz.pdp.dao;
//Sevinch Abdisattorova 02/07/2022 9:18 AM


import uz.pdp.model.Task;
import uz.pdp.util.Constants;
import uz.pdp.util.DBConnect;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class TaskDao {

    public static List<Task> getAllTasks(int page) {

        List<Task> taskList = new ArrayList<>();
        Connection connection = DBConnect.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement("SELECT * from tasks order by deadline limit " +
                    Constants.num_in_a_page + " offset " +
                    Constants.num_in_a_page * page);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                taskList.add(new Task(resultSet.getInt("id"),
                        resultSet.getInt("user_id"),
                        resultSet.getString("title"),
                        resultSet.getString("description"),
                        resultSet.getBoolean("status"),
                        resultSet.getTimestamp("deadline").toLocalDateTime(),
                        resultSet.getTimestamp("created_at").toLocalDateTime(),
                        resultSet.getTimestamp("updated_at").toLocalDateTime())
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return taskList;
    }

    public static List<Task> getAllTasksOfUser(int page, String username) {

        List<Task> taskList = new ArrayList<>();
        Connection connection = DBConnect.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement("SELECT * from tasks " +
                    " join users u on tasks.user_id =u.id " +
                    "where username =?" +
                    "order by deadline limit " +
                    Constants.num_in_a_page + " offset " +
                    Constants.num_in_a_page * page);
            preparedStatement.setString(1, username);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                taskList.add(new Task(resultSet.getInt("id"),
                        resultSet.getInt("user_id"),
                        resultSet.getString("title"),
                        resultSet.getString("description"),
                        resultSet.getBoolean("status"),
                        resultSet.getTimestamp("deadline").toLocalDateTime(),
                        resultSet.getTimestamp("created_at").toLocalDateTime(),
                        resultSet.getTimestamp("updated_at").toLocalDateTime())
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return taskList;
    }

    public static Task getTaskById(int id) {

        Connection connection = DBConnect.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement("SELECT * from tasks where id =" + id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                return (new Task(resultSet.getInt("id"),
                        resultSet.getInt("user_id"),
                        resultSet.getString("title"),
                        resultSet.getString("description"),
                        resultSet.getBoolean("status"),
                        resultSet.getTimestamp("deadline").toLocalDateTime(),
                        resultSet.getTimestamp("created_at").toLocalDateTime(),
                        resultSet.getTimestamp("updated_at").toLocalDateTime()));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int countTasks() {
        Connection connection = DBConnect.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement("SELECT count (*) from tasks ");
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("count");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static int countTasksOfUser(String username) {
        Connection connection = DBConnect.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement("SELECT count (*) from tasks " +
                    " join users u on tasks.user_id =u.id " +
                    "where username =?");
            preparedStatement.setString(1, username);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("count");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static boolean addTask(String title, String description, LocalDateTime deadline, boolean res, int userId) {
        Connection connection = DBConnect.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("Insert  into tasks (title,description,deadline,status,user_id) values " +
                    "('" + title + "','" + description + "','" + deadline + "'," + res + ","+userId+")");
            preparedStatement.execute();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean editTask(int id, String title, String description, LocalDateTime deadline, boolean res) {
        Connection connection = DBConnect.getConnection();
        try {
            PreparedStatement preparedStatement =
                    connection.prepareStatement("update tasks set title = '" + title + "'," +
                            "                 description ='" + description + "'," +
                            "                 deadline = '" + deadline + "' ," +
                            " status= " + res +
                            " where id =" + id);
            preparedStatement.execute();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }


    public static boolean deleteTask(int id) {
        Connection connection = DBConnect.getConnection();
        try {
            PreparedStatement preparedStatement =
                    connection.prepareStatement(" delete  from tasks " +
                            "where id =" + id);
            preparedStatement.execute();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
