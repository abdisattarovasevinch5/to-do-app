package uz.pdp.dao;
//Sevinch Abdisattorova 02/07/2022 11:29 PM


import uz.pdp.model.User;
import uz.pdp.util.DBConnect;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDao {

    public static List<User> getAll() {
        Connection connection = DBConnect.getConnection();
        List<User> userList = new ArrayList<>();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from users");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                userList.add(new User(
                                resultSet.getInt("id"),
                                resultSet.getString("full_name"),
                                resultSet.getString("username"),
                                resultSet.getString("password")
                        )
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userList;
    }

    public static User getAuthUser(String username, String password) {

        Connection connection = DBConnect.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(" SELECT * from users where username = ? and password =?");
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);


            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return new User(resultSet.getInt("id"),
                        resultSet.getString("full_name"),
                        resultSet.getString("username"),
                        resultSet.getString("password")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static User getUser(String username) {

        Connection connection = DBConnect.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(" SELECT * from users where username = ? ");
            preparedStatement.setString(1, username);

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return new User(resultSet.getInt("id"),
                        resultSet.getString("full_name"),
                        resultSet.getString("username"),
                        resultSet.getString("password")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean addUser(String full_name, String username, String password) {

        Connection connection = DBConnect.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("Insert into users(full_name,username,password) " +
                    "values (?,?,?)");
            preparedStatement.setString(1, full_name);
            preparedStatement.setString(2, username);
            preparedStatement.setString(3, password);

            preparedStatement.execute();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean deleteUser(int id) {
        Connection connection = DBConnect.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE from users where id =" + id);
            preparedStatement.execute();

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
