package uz.pdp.model;
//Sevinch Abdisattorova 02/07/2022 9:12 AM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Task {
    private int id;
    private int userId;
    private String title;
    private String description;
    private boolean status;
    private LocalDateTime deadline;
    private LocalDateTime created_at = LocalDateTime.now();
    private LocalDateTime updated_at = LocalDateTime.now();
}
