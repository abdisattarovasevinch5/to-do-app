package uz.pdp.model;
//Sevinch Abdisattorova 02/07/2022 11:29 PM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class User {
    private int id;
    private String fullName;
    private String username;
    private String password;
    ;
}
