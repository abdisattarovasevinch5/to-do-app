package uz.pdp.controller;
//Sevinch Abdisattorova 02/08/2022 10:43 PM


import uz.pdp.dao.UserDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/deleteUser")
public class DeleteUser extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String sid = req.getParameter("id");

        boolean isDeleted = UserDao.deleteUser(Integer.parseInt(sid));

        String res = "";
        if (isDeleted) {
            res = "Successfully deleted!";
        } else res = " Error! Not deleted!";

        resp.sendRedirect("/main.jsp?message=" + res + "&success=" + isDeleted);


    }
}
