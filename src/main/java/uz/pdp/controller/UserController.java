package uz.pdp.controller;
//Sevinch Abdisattorova 02/08/2022 9:22 AM

import uz.pdp.dao.UserDao;
import uz.pdp.model.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/users")
public class UserController extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String full_name = req.getParameter("full_name");
        String username = req.getParameter("username");
        String password = req.getParameter("password");

        boolean isAdded = UserDao.addUser(full_name, username, password);
        String result = "";
        if (isAdded) {
            result = "Successfully added!";
        } else
            result = "Error! Not added!";
        resp.sendRedirect("/main.jsp?message=" + result + "&success=" + isAdded);

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Cookie[] cookies = req.getCookies();
        String username = "";
        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                for (User user : UserDao.getAll()) {
                    if (user.getUsername().equals(cookies[i].getValue())) {
                        username = user.getUsername();
                        break;
                    }
                }
            }
        }
        User user = UserDao.getUser(username);

        req.setAttribute("user", user);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/settings.jsp");
        requestDispatcher.forward(req, resp);

    }
}
