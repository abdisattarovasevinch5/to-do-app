package uz.pdp.controller;
//Sevinch Abdisattorova 02/07/2022 11:27 PM


import uz.pdp.dao.UserDao;
import uz.pdp.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");

        User authUser = UserDao.getAuthUser(username, password);
        boolean success = false;
        String message = "";
        if (Objects.nonNull(authUser)) {
            String fullName = authUser.getFullName();
            success = true;
            message = fullName + ",you are currently logged in!";
            Cookie cookie = new Cookie("name", username);
            resp.addCookie(cookie);
            resp.sendRedirect("/main.jsp?message=" + message + "&success=" + success+"&login=true");
        } else {
            message = "Username or password incorrect!";
            resp.sendRedirect("/main.jsp?message=" + message + "&success=" + success+"login=false");
        }
    }
}

