package uz.pdp.controller;
//Sevinch Abdisattorova 02/04/2022 11:24 AM


import uz.pdp.dao.TaskDao;
import uz.pdp.dao.UserDao;
import uz.pdp.model.Task;
import uz.pdp.model.User;
import uz.pdp.util.Constants;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

@WebServlet("/tasks")
public class TaskController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        Cookie[] cookies = req.getCookies();
        String username = "";
        boolean logged_in = false;
        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                for (User user : UserDao.getAll()) {
                    if (user.getUsername().equals(cookies[i].getValue())) {
                        username = user.getUsername();
                        logged_in = true;
                        break;
                    }
                }
            }
        }
        if (logged_in) {
            String pageStr = req.getParameter("page");
            int page = 0;
            if (pageStr != null) {
                page = Integer.parseInt(pageStr);
            }
            List<Task> allTasks = TaskDao.getAllTasksOfUser(page, username);

            int countTasks = TaskDao.countTasksOfUser(username);

            int pages = countTasks / Constants.num_in_a_page;

            int remainder = countTasks % Constants.num_in_a_page;

            if (remainder != 0) {
                pages += 1;
            }
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/jsp/viewTasks.jsp");

            req.setAttribute("pages", pages);


            req.setAttribute("taskList", allTasks);

            requestDispatcher.forward(req, resp);
        } else {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/main.jsp?message=Please,login first!&success=false&login=false");
            requestDispatcher.forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String title = req.getParameter("title");
        String description = req.getParameter("description");
        LocalDateTime deadline = LocalDateTime.parse(req.getParameter("deadline"));
        String status = req.getParameter("status");
        String idStr = req.getParameter("id");
        boolean res = status != null;

        Cookie[] cookies = req.getCookies();
        int userId = 0;
        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                for (User user : UserDao.getAll()) {
                    if (user.getUsername().equals(cookies[i].getValue())) {
                        userId = user.getId();
                        break;
                    }
                }
            }

            String result = "";
            if (idStr != null) {
                int id = Integer.parseInt(idStr);
                boolean isEdited = TaskDao.editTask(id, title, description, deadline, res);
                if (isEdited) {
                    result = "Successfully edited";
                } else result = " Error! Not edited!";
                resp.sendRedirect("/tasks?message=" + result + "&success=" + isEdited);
            } else {
                boolean isAdded = TaskDao.addTask(title, description, deadline, res, userId);
                if (isAdded) {
                    result = "Successfully added!";
                } else
                    result = "Error! Not added!";
                resp.sendRedirect("/tasks?message=" + result + "&success=" + isAdded);
            }
        }
    }
}
