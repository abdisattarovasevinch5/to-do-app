package uz.pdp.controller;
//Sevinch Abdisattorova 02/07/2022 9:32 AM


import uz.pdp.dao.TaskDao;
import uz.pdp.model.Task;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/editTask")
public class EditTask extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String idStr = req.getParameter("id");

        int id = Integer.parseInt(idStr);

        Task taskById = TaskDao.getTaskById(id);

        req.setAttribute("task", taskById);

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("jsp/task-form.jsp");

        requestDispatcher.forward(req, resp);
    }
}
