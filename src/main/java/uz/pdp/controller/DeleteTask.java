package uz.pdp.controller;
//Sevinch Abdisattorova 02/07/2022 9:56 AM


import uz.pdp.dao.TaskDao;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/deleteTask")
public class DeleteTask extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        int id = Integer.parseInt(req.getParameter("id"));

        boolean isDeleted = TaskDao.deleteTask(id);
        String res = "";
        if (isDeleted) {
            res = "Successfully deleted!";
        } else res = " Error! Not deleted!";

        resp.sendRedirect("/tasks?message=" + res + "&success=" + isDeleted);

    }

}
