package uz.pdp.controller;
//Sevinch Abdisattorova 02/08/2022 12:01 AM


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Cookie ck = new Cookie("name", "");
        ck.setMaxAge(0);
        resp.addCookie(ck);

        String message = "You are currently logged out!";

        resp.sendRedirect("/main.jsp?message=" + message + "&success=true");
    }
}
