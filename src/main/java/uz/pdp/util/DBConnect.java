package uz.pdp.util;
//Sevinch Abdisattorova 02/07/2022 9:19 AM


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static uz.pdp.util.Constants.*;

public class DBConnect {
    public static Connection getConnection() {

        try {
            Class.forName("org.postgresql.Driver");
            Connection connection = DriverManager.getConnection(url, username, password);
            return connection;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
